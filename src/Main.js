import React, {Component} from "react";
import {HashRouter, Route} from "react-router-dom";
import Splash from "./components/Splash";
import Generate from "./components/Generate";
import Loading from "./components/Loading";
import Playlist from "./components/Playlist";
import About from "./components/About";

import './App.scss';

class Main extends Component {
	render() {
		return (
			<HashRouter>
				<div className="content">
					<Route exact path="/" component={Splash}/>
					<Route path="/generate" component={Generate}/>
					<Route path="/loading" component={Loading}/>
					<Route path="/playlist" component={Playlist}/>
					<Route path="/about" component={About}/>
				</div>
			</HashRouter>
		);
	}
}

export default Main;
