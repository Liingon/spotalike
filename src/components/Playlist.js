import React, {Component} from "react";
import MenuBar from "./MenuBar";
import tracks from "../tracks";
import Track from "./Track";

class Playlist extends Component {
	render() {
		return (
			<div className={"page playlist"}>
				<div className={"head"}>
					<MenuBar to={"/generate"} heading={"Little Lion Man"} showMore={true}/>

					<div className={"playlist-info"}>
						<i className="material-icons">queue_music</i><span>{tracks.length} tracks</span>
						<i className="material-icons">access_time</i><span>3 h 43 min</span>
					</div>
				</div>

				<div className={"main"}>
					<h2>Tracks</h2>
					<ul className={"tracks"}>
						{tracks.map((item, index) => {
							return <Track track={item.track} artist={item.artist} icon={"more_horiz"} to={"/playlist"} key={index} />
						})}
					</ul>
				</div>

				<div className={"foot"}>
					<p><img src={"./logo-spotify.png"} alt={"Spotify"}/>Enjoy with Spotify</p>
				</div>
			</div>
		);
	}
}

export default Playlist;
