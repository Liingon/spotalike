import React, {Component} from "react";
import MenuBar from "./MenuBar";

class About extends Component {
	render() {
		return (
			<div className={"page about"}>
				<div className={"head"}>
					<MenuBar to={"/generate"} heading={"About"} showMore={true}/>

					<p>Spotalike is a playlist service for Spotify based on similar songs according to Last.fm.</p>
				</div>

				<div className={"main"}>
					<h2>Spotalike</h2>
					<p>Postcards have been consistently one of the more popular collectibles and used as promotional materials. More and more business owners are turning to color postcards as a good way of promoting their products and services. This is simply because postcards don’t have to be opened. It won’t get lumped in with all the mail envelopes that people usually toss without opening.</p>
					<p>Business owners can’t afford to make mistakes when it to comes to their promotional materials. They spend a considerable amount of money in it so they would want them to bring greater sales and profits to their business. Thus, if the postcard reveals a glossy, professional attractive picture, the recipient will likely look at the picture and turn it over to see who sent it.</p>
				</div>
			</div>
		);
	}
}

export default About;
