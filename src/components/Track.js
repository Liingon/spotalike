import React, {Component} from "react";
import {NavLink} from "react-router-dom";

class Track extends Component {
	render() {
		return (
			<li>
				<NavLink to={this.props.to}>
				<span className={"track"}>{this.props.track}</span><br/>
				<span className={"artist"}>{this.props.artist}</span><br/>
				<i className="material-icons">{this.props.icon}</i>
				</NavLink>
			</li>
		);
	}
}

export default Track;
