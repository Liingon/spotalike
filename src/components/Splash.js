import React, {Component} from "react";
import {NavLink} from "react-router-dom";

class Splash extends Component {
	render() {
		// Redirect to generate after delay
		setTimeout(function () { this.props.history.push('/generate') }.bind(this), 1500);

		return (
			<div className={"splash page"}>
				<NavLink to={"/generate"}>
				<div className={"logo"}>
					<img src={"./logo-spotalike.svg"} alt={'Spotalike'}/>
				</div>

				<div className={"divider pulsate"}/>
				</NavLink>
			</div>
		);
	}
}

export default Splash;
