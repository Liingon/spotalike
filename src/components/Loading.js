import React, {Component} from "react";
import MenuBar from "./MenuBar";

class Loading extends Component {
	render() {
		// Redirect to playlist after delay
		setTimeout(function () { this.props.history.push('/playlist') }.bind(this), 1500);

		return (
			<div className={"page loading"}>
				<div className={"head"}>
					<MenuBar to={"/generate"} heading={"Loading..."}/>
				</div>

				<div className={"divider pulsate"}/>
			</div>
		);
	}
}

export default Loading;
