import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import Track from "./Track";

import tracks from "../tracks";
import Search from "./Search";

class Generate extends Component {
	render() {
		// Filter out popular tracks from json
		let popular = tracks.filter(function (el) {
			return el.popular;
		});

		return (
			<div className={"page generate"}>
				<div className={"head"}>
					<div className={"menubar"}>
						<NavLink to="/about"><i className="material-icons">info_outline</i></NavLink>
					</div>

					<div className={"logo"}>
						<img src={"./logo-spotalike.svg"} alt={'Spotalike'}/>
					</div>
				</div>

				<div className={"main"}>
					<Search tracks={tracks}/>

					<p>Give us your favourite track and we’ll serve up a sweet Spotify playlist with similar songs that you’ll love!</p>

					<h2>Popular tracks</h2>
					<ul className={"tracks"}>
						{popular.map((item, index) => {
							return <Track track={item.track} artist={item.artist} icon={"arrow_forward"} to={"/loading"} key={index} />
						})}
					</ul>
				</div>
			</div>
		);
	}
}

export default Generate;