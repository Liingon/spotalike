import React, {Component} from "react";
import Track from "./Track";

class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {filterText: ''};
		this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
		this.clearFilter = this.clearFilter.bind(this);
	}

	handleFilterTextChange(e) {
		this.setState({
			filterText: e.target.value
		});
	}

	clearFilter() {
		this.setState({
			filterText: ''
		});
	}

	hasText() {
		return this.state.filterText.length;
	}

	render() {
		// Filter json based on input
		let tracks = [];
		if (this.hasText()) {
			tracks = this.props.tracks.filter(function (el) {
				return el.track.toLowerCase().includes(this.state.filterText.toLowerCase()) || el.artist.toLowerCase().includes(this.state.filterText.toLowerCase())
			}.bind(this));
		}

		return (
			<form className={"search"}>
				<input
					type="text"
					placeholder="Enter a track"
					value={this.state.filterText}
					onChange={this.handleFilterTextChange}
				/>
				{this.hasText() ? <i className="material-icons clear" onClick={this.clearFilter}>clear</i> : null}

				<ul className={"tracks"}>
					{tracks.map((item, index) => {
						return <Track track={item.track} artist={item.artist} icon={"arrow_forward"} to={"/loading"} key={index} />
					})}
				</ul>
			</form>
		);
	}
}

export default Search;
