import React, {Component} from "react";
import {NavLink} from "react-router-dom";

class MenuBar extends Component {
	constructor(props) {
		super(props);
		this.state = {overlay: false};
		this.setOverlay = this.setOverlay.bind(this);
	}

	setOverlay(state) {
		this.setState({
			overlay: state
		});
	}

	render() {
		return (
			<div className={"menubar"}>
				<NavLink to={this.props.to} className={"back"}>
					<i className="material-icons">arrow_backward</i>
				</NavLink>

				<h1>{this.props.heading}</h1>

				{this.props.showMore ?
					<span onClick={() => this.setOverlay(true)} className={"a"}><i className="material-icons">more_horiz</i></span> : null}

				{this.state.overlay ?
					<div className={"overlay"} onClick={() => this.setOverlay(false)}>
						<div className={"popover"}>
							<div>
								Share with your friends <i className="material-icons">share</i>
							</div>

							<div>
								Rate this app <i className="material-icons">star_half</i>
							</div>
						</div>
					</div>
					: null}
			</div>
		);
	}
}

export default MenuBar;
